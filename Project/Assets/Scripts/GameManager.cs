﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves = 1 << 0,
        Dot = 1 << 1,
        Square = 1 << 2,
        LargeDiamond = 1 << 3,
        SmallDiamonds = 1 << 4,
        Command = 1 << 5,
        Bomb = 1 << 6,
        Sun = 1 << 7,
        Bones = 1 << 8,
        Drop = 1 << 9,
        Face = 1 << 10,
        Hand = 1 << 11,
        Flag = 1 << 12,
        Disk = 1 << 13,
        Candle = 1 << 14,
        Wheel = 1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    public int gamesWon;
    public int tilesFlipped;
    public int loadingErrors;
    public int matchesMade;
    public float timeCounted;

    public int minutesPlayed;

    private void Start()
    {
        LoadLevel(CurrentLevelNumber);
    }

    void Update()
    {
        timeCounted += Time.deltaTime;
        if (timeCounted == 60)
        {
            minutesPlayed++;

            timeCounted = 0;
        }
        
    }

    //initial setup. This loads all the resources we need to build the game
    private void LoadLevel(int levelNumber)
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        //gets the symbols we defined earlier and organizes them
        List<Symbols> symbols = GetRequiredSymbols(level);

        //shifts each tile selection over by 1 with each row defined to create rows
        for (int rowIndex = 0; rowIndex < level.Rows; ++rowIndex)
        {
            float yPosition = rowIndex * (1 + TileSpacing);
            //same but with columns
            for (int colIndex = 0; colIndex < level.Columns; ++colIndex)
            {

                

                //space between tiles
                float xPosition = colIndex * (1 + TileSpacing);
                GameObject tileObject = Instantiate(TilePrefab, new Vector3(xPosition, yPosition, 0), Quaternion.identity, this.transform);

                //randomly place symbols
                int symbolIndex = UnityEngine.Random.Range(0, symbols.Count);
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol(symbols[symbolIndex]);
                tileObject.GetComponent<Tile>().Initialize(this, symbols[symbolIndex]);

                //cleanup after adding symbol
                symbols.RemoveAt(symbolIndex);
            }
        }

        SetupCamera(level);
    }

    //gets symbols from the list we define in the level creator
    private List<Symbols> GetRequiredSymbols(LevelDescription level)
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //makes sure the card grid is completely square. If not then it wouldn't be usable
        {
            Array allSymbols = Enum.GetValues(typeof(Symbols));

            m_cardsRemaining = cardTotal;
            //there HAS to be an even number of cards because we can only get rid of them by matching pairs
            if (cardTotal % 2 > 0)
            {
                new ArgumentException("There must be an even number of cards");
                loadingErrors++;
                AnalyticsEvent.Custom("LoadingError", new Dictionary<string, object>
                    {{"LoadingError", loadingErrors },
                    });
            }
            //which symbols are used
            foreach (Symbols symbol in allSymbols)
            {
                if ((level.UsedSymbols & symbol) > 0)
                {
                    symbols.Add(symbol);
                }
            }
        }

        //we NEED symbols to create a new level
        {
            if (symbols.Count == 0)
            {
                new ArgumentException("The level has no symbols set");
            }

            //self explanitory
            if (symbols.Count > cardTotal / 2)
            {
                new ArgumentException("There are too many symbols for the number of cards.");
            }
        }

        //this next section makes sure that we don't get too many of the same symbol
        {
            int repeatCount = (cardTotal / 2) - symbols.Count;
            if (repeatCount > 0)
            {
                //these keep track of the uses of each symbol
                List<Symbols> symbolsCopy = new List<Symbols>(symbols);
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for (int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex)
                {
                    int randomIndex = UnityEngine.Random.Range(0, symbolsCopy.Count);
                    duplicateSymbols.Add(symbolsCopy[randomIndex]);
                    symbolsCopy.RemoveAt(randomIndex);
                    if (symbolsCopy.Count == 0)
                    {
                        symbolsCopy.AddRange(symbols);
                    }
                }
                symbols.AddRange(duplicateSymbols);
            }
        }
        //AddRange limits the size of the list, in this case to the predefined parameters
        symbols.AddRange(symbols);

        return symbols;
    }

    //this section deals with the reading of the sprite image for the symbols on the cards. The card symbols are laid out
    //in a 4x4 grid on the card, hence why we are using modulus (to make things precise by handling the remainder)
    private Vector2 GetOffsetFromSymbol(Symbols symbol)
    {
        Array symbols = Enum.GetValues(typeof(Symbols));
        for (int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex)
        {
            if ((Symbols)symbols.GetValue(symbolIndex) == symbol)
            {
                return new Vector2(symbolIndex % 4, symbolIndex / 4) / 4f;
            }
        }
        //there are 12 images in total, we look for one which isn't there and we get this
        Debug.Log("Failed to find symbol");
        return Vector2.zero;
    }

    //camera settings so we don't have to set them manually on each level. Takes the size of the playing area (in regards to the number of cards) into account.
    private void SetupCamera(LevelDescription level)
    {
        Camera.main.orthographicSize = (level.Rows + (level.Rows + 1) * TileSpacing) / 2;
        Camera.main.transform.position = new Vector3((level.Columns * (1 + TileSpacing)) / 2, (level.Rows * (1 + TileSpacing)) / 2, -10);
    }

    //this calls functions which flip over the tiles when we run through this. It is important to know that this one encapsulating if statement checks if tiles have ALREADY been flipped.
    public void TileSelected(Tile tile)
    {
        if (m_tileOne == null)
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
            tilesFlipped++;
            AnalyticsEvent.Custom("TileFlipped", new Dictionary<string, object>
                    {{"TileFlipped", tilesFlipped },});
        }
        else if (m_tileTwo == null)
        {
            //else if this is called and we don't have anything stored in tile two we proceed to put our selection in "tile"
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            tilesFlipped++;
            AnalyticsEvent.Custom("TileFlipped", new Dictionary<string, object>
                    {{"TileFlipped", tilesFlipped },});
            //FLIP!!!
            if (m_tileOne.Symbol == m_tileTwo.Symbol)
            {
                //if they are both filled
                StartCoroutine(WaitForHide(true, 1f));
            }
            else
            {
                //stop until we fill both
                StartCoroutine(WaitForHide(false, 1f));
            }
        }
    }

    //keeps track of which level we are on
    private void LevelComplete()
    {

        AnalyticsEvent.Custom("gameTimeInMinutes", new Dictionary<string, object>
                    {{"gameTimeInMinutes", minutesPlayed },
                    });

        ++CurrentLevelNumber;
        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    //flips tiles back to their original state
    private IEnumerator WaitForHide(bool match, float time)
    {
        float timer = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer >= time)
            {
                break;
            }
            yield return null;
        }
        if (match)
        {
            //if we match two tiles
            Destroy(m_tileOne.gameObject);
            Destroy(m_tileTwo.gameObject);
            m_cardsRemaining -= 2;
            matchesMade++;
            AnalyticsEvent.Custom("MatchesMade", new Dictionary<string, object>
                    {{"MatchesMade", matchesMade },
                    });
        }
        else
        {
            //if we're a fuckup and DON'T match the tiles
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }

        //this clears the list so we don't have to worry about the selection glitching out
        m_tileOne = null;
        m_tileTwo = null;

        //if we've exterminated all of the cards in the scene we are promoted to the next level
        if (m_cardsRemaining == 0)
        {
            print("You won!");

            gamesWon++;
                    AnalyticsEvent.Custom("GameWon", new Dictionary<string, object>
                    {{"GameWon", gamesWon },
                    });               
            
            LevelComplete();
        }
    }
}
