﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //this script deals with various functions which are called in the game manager
    private void Start()
    {
        //gets the child of the tile in question. This means we can flip the mesh with a material on it if we want to
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //testing if we are clicking
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //defines manager and symbol as different names
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //flips cards over (using spin) at a 180 degree angle. Does it in a 0.8th of a second
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //flips back. This is more or less the same as the last function
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //flips the tile with parameters tied to time. This controls the animation.
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
